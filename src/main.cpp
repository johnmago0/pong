#include <raylib.h>

int playerScore = 0;
int cpuScore = 0;

class Ball
{
public:
  float x, y;
  int speedX, speedY;
  int radius;

  void Draw() { DrawCircle(x, y, radius, WHITE); }

  void Update()
  {
    x += speedX;
    y += speedY;

    if (y + radius >= GetScreenHeight() || y - radius <= 0) { speedY *= -1; }

    if (y + radius >= GetScreenWidth()) {
      cpuScore++;
      ResetBall();
    }


    if (x + radius >= GetScreenWidth()) { speedX *= -1; }

    if (x - radius <= 0) {
      playerScore++;
      ResetBall();
    }
  }


  void ResetBall()
  {
    x = GetScreenWidth() / 2;
    y = GetScreenHeight() / 2;

    int speedChoices[2] = { -1, 1 };
    speedX *= speedChoices[GetRandomValue(0, 1)];
    speedY *= speedChoices[GetRandomValue(0, 1)];
  }
};

class Paddle
{
public:
  float x, y;
  float width, height;
  int speed;

  void Draw() { DrawRectangle(x, y, width, height, WHITE); }

  void Update()
  {
    if (IsKeyDown(KEY_UP)) { y -= speed; }

    if (IsKeyDown(KEY_DOWN)) { y += speed; }

    if (y <= 0) { y = 0; }

    if (y + height >= GetScreenHeight()) { y = GetScreenHeight() - height; }
  }
};

class CpuPaddle : public Paddle
{
public:
  void Update(int ballY)
  {
    if (y + height / 2 > ballY) { y -= speed; }

    if (y + height / 2 <= ballY) { y += speed; }

    if (y <= 0) { y = 0; }

    if (y + height >= GetScreenHeight()) { y = GetScreenHeight() - height; }
  }
};

int main()
{
  const int screenWidth = 1280;
  const int screenHeight = 800;
  const int rectangleHeight = 90;
  InitWindow(screenWidth, screenHeight, "Pong");
  SetTargetFPS(60);

  Ball ball;
  ball.radius = 20;
  ball.x = screenWidth / 2;
  ball.y = screenHeight / 2;
  ball.speedX = 6;
  ball.speedY = 6;

  Paddle player;
  player.x = 10;
  player.y = (screenHeight / 2) - (rectangleHeight / 2);
  player.width = 15;
  player.height = rectangleHeight;
  player.speed = 6;

  CpuPaddle cpu;
  cpu.x = screenWidth - 25;
  cpu.y = screenHeight / 2 - (rectangleHeight / 2);
  cpu.width = 15;
  cpu.height = rectangleHeight;
  cpu.speed = 6;

  while (!WindowShouldClose()) {
    BeginDrawing();

    ball.Update();
    player.Update();
    cpu.Update(ball.y);

    if (CheckCollisionCircleRec(Vector2{ ball.x, ball.y },
          ball.radius,
          Rectangle{ player.x, player.y, player.width, player.height })) {
      ball.speedX *= -1;
    }

    if (CheckCollisionCircleRec(Vector2{ ball.x, ball.y },
          ball.radius,
          Rectangle{ cpu.x, cpu.y, cpu.width, cpu.height })) {
      ball.speedX *= -1;
    }


    // The elements drawed in loop are not erased. So we need to clear the
    // background in each iteration of the loop.
    ClearBackground(BLACK);

    DrawLine(screenWidth / 2, 0, screenWidth / 2, screenHeight, WHITE);

    ball.Draw();

    // The rectangle starts its drawn from the first pixel at the top. So to
    // center the rectangle in the screen we need to get the middle of the
    // screen minus the middle of the rectangle.
    player.Draw();

    // The value 25 comes from the width of the rectangle plus the offset of the
    // rectangle and the screen
    cpu.Draw();

    DrawText(TextFormat("%i", cpuScore), screenWidth / 4 - 20, 20, 80, WHITE);
    DrawText(
      TextFormat("%i", playerScore), 3 * screenWidth / 4 - 20, 20, 80, WHITE);

    EndDrawing();
  }

  CloseWindow();
  return 0;
}